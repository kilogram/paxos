
import contextlib
import unittest

from paxos import msg, Replica


class ReplicaTest(unittest.TestCase):
    others = [
        (0, {'leader'}),
        (1, {'acceptor', 'replica'}),
        (2, {'acceptor', 'leader', 'replica'}),
        (3, {'acceptor'}),
        ]

    @contextlib.contextmanager
    def _stubs(self):
        leaders = {i[0] for i in self.others if 'leader' in i[1]}
        yield Replica(leaders)

    def _msg(self, from_, to, action, **kwargs):
        msg = dict({
            'from': from_ or 0,
            'to': to,
            'action': action,
            }, **kwargs)
        if from_ is None:
            msg.pop('from')
        return msg

    def test_init(self):
        with self._stubs() as replica:
            pass

    def test_recovery(self):
        with self._stubs() as replica:
            resp = replica.recover([])

            # Make request 'cmd0', get proposal (0 -> 'cmd0') to primary leader
            resp = replica.deliver(
                self._msg(1, 0, msg.request, command='cmd0'))
            self.assertCountEqual(
                [self._msg(None, i, msg.propose, slot=0, command='cmd0')
                 for i in {0, 2}],
                resp)

            state = replica.get_state()
            replica.recover([state])

            # Decision (0 -> 'cmd1'), no re-proposal (we lost state)
            resp = replica.deliver(
                self._msg(1, 0, msg.decision, command='cmd1', slot=0))
            self.assertCountEqual([{
                'action': msg.response,
                'slot': 0,
                'command': 'cmd1',
                }],
                resp)

    def test_propose(self):
        with self._stubs() as replica:
            resp = replica.recover([])

            # Make request 'cmd0', get proposal (0 -> 'cmd0') to all leaders
            resp = replica.deliver(
                self._msg(1, 0, msg.request, command='cmd0'))
            self.assertCountEqual([
                self._msg(None, i, msg.propose, slot=0, command='cmd0')
                for i in {0, 2}],
                resp)

            # Decision (0 -> 'cmd0'), respond 'cmd0'
            resp = replica.deliver(
                self._msg(1, 0, msg.decision, command='cmd0', slot=0))
            self.assertCountEqual([{
                'action': msg.response,
                'command': 'cmd0',
                'slot': 0,
                }],
                resp)

    def test_repropose(self):
        with self._stubs() as replica:
            resp = replica.recover([])

            # Make request 'cmd0', get proposal (0 -> 'cmd0') to all leaders
            resp = replica.deliver(
                self._msg(1, 0, msg.request, command='cmd0'))
            self.assertCountEqual([
                self._msg(None, i, msg.propose, slot=0, command='cmd0')
                for i in {0, 2}],
                resp)

            # Make request 'cmd2', get proposal (1 -> 'cmd2') to all leaders
            resp = replica.deliver(
                self._msg(1, 0, msg.request, command='cmd2'))
            self.assertCountEqual([
                self._msg(None, i, msg.propose, slot=1, command='cmd2')
                for i in {0, 2}],
                resp)

            # Decision (0 -> 'cmd1'), re-propose (0 -> 'cmd0'), respond 'cmd1'
            resp = replica.deliver(
                self._msg(1, 0, msg.decision, command='cmd1', slot=0))
            self.assertCountEqual([
                self._msg(None, i, msg.propose, slot=2, command='cmd0')
                for i in {0, 2}] + [{
                    'action': msg.response,
                    'slot': 0,
                    'command': 'cmd1',
                }],
                resp)
