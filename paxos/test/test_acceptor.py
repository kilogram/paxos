
import unittest

from paxos import msg
from paxos import Acceptor


class AcceptorTest(unittest.TestCase):
    def test_init(self):
        Acceptor()

    def test_prepare(self):
        acc = Acceptor()
        resp = acc.deliver({
            'from': 0,
            'action': msg.prepare,
            'ballot': 0,
            })
        self.assertListEqual([{
            'accepted': [],
            'to': 0,
            'ballot': 0,
            'action': msg.prepare
            }], resp)

        resp = acc.deliver({
            'from': 1,
            'action': msg.prepare,
            'ballot': 1,
            })
        self.assertListEqual([{
            'accepted': [],
            'to': 1,
            'ballot': 1,
            'action': msg.prepare
            }], resp)

        resp = acc.deliver({
            'from': 0,
            'action': msg.prepare,
            'ballot': 0,
            })
        self.assertListEqual([{
            'accepted': [],
            'to': 0,
            'ballot': 1,
            'action': msg.prepare
            }], resp)

    def test_accept(self):
        acc = Acceptor()
        acc.deliver({
            'from': 0,
            'action': msg.prepare,
            'ballot': 0,
            })

        resp = acc.deliver({
            'from': 0,
            'action': msg.accept,
            'ballot': 0,
            'slot': 0,
            'command': 'cmd0',
            })
        self.assertListEqual([{
            'action': msg.accept,
            'to': 0,
            'ballot': 0,
            'slot': 0,
            }], resp)

        resp = acc.deliver({
            'from': 1,
            'action': msg.prepare,
            'ballot': 1,
            })
        self.assertListEqual([{
            'action': msg.prepare,
            'to': 1,
            'ballot': 1,
            'accepted': [(0, 0, 'cmd0')],
            }], resp)

    def test_overlap(self):
        acc = Acceptor()
        acc.deliver({
            'from': 0,
            'action': msg.prepare,
            'ballot': 0,
            })
        acc.deliver({
            'from': 1,
            'action': msg.prepare,
            'ballot': 1,
            })

        resp = acc.deliver({
            'from': 0,
            'action': msg.accept,
            'ballot': 0,
            'slot': 0,
            'command': 'cmd0',
            })
        self.assertListEqual([{
            'action': msg.accept,
            'to': 0,
            'ballot': 1,
            'slot': 0,
            }], resp)

        resp = acc.deliver({
            'from': 0,
            'action': msg.prepare,
            'ballot': 2,
            })
        self.assertListEqual([{
            'action': msg.prepare,
            'to': 0,
            'ballot': 2,
            'accepted': [],
            }], resp)

        resp = acc.deliver({
            'from': 0,
            'action': msg.accept,
            'ballot': 2,
            'slot': 0,
            'command': 'cmd0',
            })
        self.assertListEqual([{
            'action': msg.accept,
            'to': 0,
            'ballot': 2,
            'slot': 0,
            }], resp)

        resp = acc.deliver({
            'from': 1,
            'action': msg.prepare,
            'ballot': 2,
            })
        self.assertListEqual([{
            'action': msg.prepare,
            'to': 1,
            'ballot': 2,
            'accepted': [(0, 2, 'cmd0')],
            }], resp)
