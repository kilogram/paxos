
import contextlib
import itertools as it
import unittest

from paxos import msg, Leader, Preempted
from paxos.common import Accepted


class LeaderTest(unittest.TestCase):
    others = [
        (0, {'leader'}),
        (1, {'acceptor', 'replica'}),
        (2, {'acceptor', 'leader', 'replica'}),
        (3, {'acceptor'}),
        ]

    @contextlib.contextmanager
    def _stubs(self):
        leaders = {i[0] for i in self.others if 'leader' in i[1]}
        acceptors = {i[0] for i in self.others if 'acceptor' in i[1]}
        replicas = {i[0] for i in self.others if 'replica' in i[1]}
        yield Leader(leaders, acceptors, replicas, it.count(0, 2))

    def _msg(self, from_, to, action, **kwargs):
        msg = dict({
            'from': from_ or 0,
            'to': to,
            'action': action,
            }, **kwargs)
        if from_ is None:
            msg.pop('from')
        return msg

    def test_init(self):
        with self._stubs() as ldr:
            pass

    def test_propose(self):
        with self._stubs() as ldr:
            ldr.recover([])
            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=2)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept,
                          ballot=2, slot=0, command='cmd0')
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.accept, ballot=2, slot=0))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.accept, ballot=2, slot=0))
            self.assertCountEqual([
                self._msg(None, i, msg.decision, slot=0, command='cmd0')
                for i in [0, 1, 2, 2]],
                resp)

    def test_propose_inactive(self):
        with self._stubs() as ldr:
            ldr.recover([])
            resp = ldr.deliver(
                self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            self.assertCountEqual([], resp)

            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=2)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept,
                          ballot=2, slot=0, command='cmd0')
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.accept, ballot=2, slot=0))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.accept, ballot=2, slot=0))
            self.assertCountEqual([
                self._msg(None, i, msg.decision, slot=0, command='cmd0')
                for i in [0, 1, 2, 2]],
                resp)

    def test_recovery(self):
        with self._stubs() as ldr:
            ldr.recover([])
            resp = ldr.reactivate()

    def test_init_preempted(self):
        with self._stubs() as ldr:
            ldr.recover([])
            ldr.reactivate()

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([], resp)

            # We attempt to catch up to the current ballot
            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=3, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=4)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=4, accepted=[]))
            self.assertCountEqual([], resp)

            with self.assertRaises(Preempted):
                ldr.deliver(
                    self._msg(2, 0, msg.prepare, ballot=5, accepted=[]))

            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=6)
                for i in range(1, 4)],
                resp)

    def test_accept_preempted(self):
        with self._stubs() as ldr:
            ldr.recover([])
            ldr.reactivate()
            ldr.deliver(self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            ldr.deliver(self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept,
                          ballot=2, slot=0, command='cmd0')
                for i in range(1, 4)],
                resp)

            ldr.deliver(self._msg(1, 0, msg.accept, ballot=2, slot=0))

            with self.assertRaises(Preempted):
                resp = ldr.deliver(
                    self._msg(2, 0, msg.accept, ballot=3, slot=0, accepted=[]))

            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=4)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=4, accepted=[]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=4, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept,
                          ballot=4, slot=0, command='cmd0')
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.accept, ballot=4, slot=0))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(3, 0, msg.accept, ballot=4, slot=0))
            self.assertCountEqual([
                self._msg(None, i, msg.decision, slot=0, command='cmd0')
                for i in [0, 1, 2, 2]],
                resp)

    def test_preempted_new_proposal(self):
        with self._stubs() as ldr:
            ldr.recover([])
            ldr.reactivate()
            ldr.deliver(self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            ldr.deliver(self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))

            new_prop = Accepted(slot=0, ballot=5, command='cmda')
            ldr.deliver(self._msg(2, 0, msg.prepare, ballot=3, accepted=[]))
            ldr.deliver(self._msg(1, 0, msg.prepare, ballot=4, accepted=[]))
            with self.assertRaises(Preempted):
                ldr.deliver(self._msg(2, 0, msg.prepare,
                                      ballot=5, accepted=[new_prop]))

            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=6)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=6, accepted=[new_prop]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=6, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept, ballot=6,
                          slot=0, command='cmda')
                for i in range(1, 4)],
                resp)

    def test_multiple_proposal_preempted_multiple_new_proposal(self):
        with self._stubs() as ldr:
            ldr.recover([])
            ldr.reactivate()
            ldr.deliver(self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            ldr.deliver(self._msg(1, 0, msg.propose, slot=1, command='cmd1'))
            ldr.deliver(self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))
            ldr.deliver(self._msg(2, 0, msg.prepare, ballot=3, accepted=[]))
            ldr.deliver(self._msg(1, 0, msg.prepare, ballot=4, accepted=[]))

            new_props = [
                Accepted(slot=0, ballot=5, command='cmda'),
                Accepted(slot=1, ballot=5, command='cmdb'),
            ]

            with self.assertRaises(Preempted):
                resp = ldr.deliver(
                    self._msg(2, 0, msg.prepare, ballot=5,
                              accepted=new_props[0:1]))

            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=6)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=6,
                          accepted=new_props[0:1]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=6,
                          accepted=new_props[0:2]))
            self.assertCountEqual(
                [
                    self._msg(None, i, msg.accept, ballot=6,
                              slot=0, command='cmda')
                    for i in range(1, 4)] +
                [
                    self._msg(None, i, msg.accept, ballot=6,
                              slot=1, command='cmdb')
                    for i in range(1, 4)],
                resp)

    def test_timebomb(self):
        with self._stubs() as ldr:
            ldr.recover([])
            ldr.set_timebomb(3)
            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=2)
                for i in range(1, 4)][:4],
                resp)

            with self.assertRaises(StopIteration):
                ldr.deliver(
                    self._msg(1, 0, msg.propose, slot=0, command='cmd0'))

        with self._stubs() as ldr:
            ldr.recover([])
            ldr.set_timebomb(6)
            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=2)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept,
                          ballot=2, slot=0, command='cmd0')
                for i in range(1, 4)],
                resp)

            with self.assertRaises(StopIteration):
                ldr.deliver(self._msg(1, 0, msg.accept, ballot=2, slot=0))

        with self._stubs() as ldr:
            ldr.recover([])
            ldr.set_timebomb(5)
            resp = ldr.deliver(
                self._msg(1, 0, msg.propose, slot=0, command='cmd0'))
            self.assertCountEqual([], resp)

            resp = ldr.reactivate()
            self.assertCountEqual([
                self._msg(None, i, msg.prepare, ballot=2)
                for i in range(1, 4)],
                resp)

            resp = ldr.deliver(
                self._msg(1, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([], resp)

            resp = ldr.deliver(
                self._msg(2, 0, msg.prepare, ballot=2, accepted=[]))
            self.assertCountEqual([
                self._msg(None, i, msg.accept,
                          ballot=2, slot=0, command='cmd0')
                for i in range(1, 4)][:2],
                resp)

            with self.assertRaises(StopIteration):
                ldr.deliver(self._msg(1, 0, msg.accept, ballot=2, slot=0))
