
import enum
import itertools as it
import logging
from operator import attrgetter

from . import common
from .common import msg, Accepted
import utils


class Preempted(Exception):
    def __init__(self, ballot, *args, **kwargs):
        super(Preempted, self).__init__(*args, **kwargs)
        self.ballot = ballot


class Commander(object):
    def __init__(self, leader, slot):
        self.log = logging.getLogger(__name__)
        self.log.addHandler(leader._hndlr)

        self.ballot = leader.ballot
        self.slot = slot
        self.waitfor = set(leader.acceptors)
        self.minority = len(self.waitfor) / 2

    def accepted(self, message):
        assert(message['slot'] == self.slot)

        if message['ballot'] < self.ballot:
            pass

        elif message['ballot'] == self.ballot:
            self.waitfor -= {message['from']}
            if len(self.waitfor) < self.minority:
                return True

        else:
            raise Preempted(message['ballot'])

        return False

    def start(self, command):
        self.log.info("Dispatching commander for %s -> %s", self.slot, command)
        return [{
            'action': msg.accept,
            'slot': self.slot,
            'ballot': self.ballot,
            'command': command,
            'to': acceptor
            }
            for acceptor in self.waitfor]

    def recovered(self, message):
        if message['from'] in self.waitfor:
            return [{
                'action': msg.accept,
                'slot': self.slot,
                'ballot': self.ballot,
                'command': command,
                'to': message['from'],
            }]


class Scout(object):
    def __init__(self, leader):
        self.log = logging.getLogger(__name__)
        self.log.addHandler(leader._hndlr)

        self.ballot = leader.ballot
        self.waitfor = set(leader.acceptors)
        self.minority = len(self.waitfor) / 2
        self.accepted = set()

    def prepared(self, message):
        if message['ballot'] < self.ballot:
            pass

        elif message['ballot'] == self.ballot:
            self.waitfor -= {message['from']}
            self.accepted |= set(message['accepted'])
            if len(self.waitfor) < self.minority:
                return True, self.accepted

        else:
            raise Preempted(message['ballot'])

        return False, None

    def start(self):
        return [{
            'action': msg.prepare,
            'ballot': self.ballot,
            'to': acceptor
            }
            for acceptor in self.waitfor]

    def recovered(self, message):
        if message['from'] in self.waitfor:
            return [{
                'action': msg.prepare,
                'ballot': self.ballot,
                'to': message['from'],
            }]


class Leader(common.PaxosActor):
    def __init__(self, leaders, acceptors, replicas, ballots, hndlr=None):
        self.log = logging.getLogger(__name__)
        self._hndlr = hndlr or logging.NullHandler()
        self.log.addHandler(self._hndlr)

        self.ballots = ballots
        self.leaders = leaders
        self.acceptors = acceptors
        self.replicas = replicas
        self.ballot = next(self.ballots)

        self.active = False
        self.proposals = utils.SparseList()
        self.decisions = utils.SparseList()
        self.scout = None

        self._routes = {
            msg.propose: self._propose,
            msg.prepare: self._prepared,
            msg.accept: self._accepted,
            msg.decision: self._decision,
            msg.recovered: self._recovered,
        }

        self.timebomb, self.count = None, None

    def set_timebomb(self, count):
        self.timebomb, self.count = count, 0

    def tick_timebomb(self, messages):
        if not self.timebomb:
            return messages
        count = self.timebomb - self.count
        self.count += min(count, len(messages))
        return messages[:count]

    def recover(self, states):
        # TODO: Recover state into current object
        try:
            to_beat = max(states)
            while self.ballot < to_beat:
                self.ballot = next(self.ballot)
        except:
            pass

    def get_state(self):
        return (self.ballot,)

    def reactivate(self):
        self.ballot = next(self.ballots)
        self.scout = Scout(self)
        self.read_ballots = True
        return self.tick_timebomb(self.scout.start())

    def all_clear(self):
        return (not self.proposals
                or all(p is None for p in self.proposals))

    def update_proposals(self, accepted):
        for slot, proposal in self.proposals.enumerate():
            if proposal is None:
                continue

            prop = max({common.Accepted(
                slot, proposal['ballot'], proposal['command'])} |
                {p for p in accepted if p.slot == slot},
                key=attrgetter('ballot'))
            proposal['command'] = prop.command

    def _propose(self, message):
        slot = message['slot']
        self.log.debug("Received proposal for %s -> %s from %s",
                       message['slot'], message['command'], message['from'])
        try:
            proposal = self.proposals[slot]
        except ValueError:
            return []

        if proposal is None:
            self.proposals[slot] = {
                'command': message['command'],
                'ballot': self.ballot,
                }

            if self.active:
                cmdr = Commander(self, slot)
                self.proposals[slot]['commander'] = cmdr
                return self.tick_timebomb(cmdr.start(message['command']))

    def _prepared(self, message):
        if not self.active and self.scout:
            self.log.debug("%s adopted %s (%s)", message['from'],
                           message['ballot'], message)
            try:
                adopted, accepted = self.scout.prepared(message)
                if adopted:
                    self.log.info("Quorum of adoptions for %s", self.ballot)
                    self.active = True
                    self.scout = None
                    self.update_proposals(accepted)
                    messages = []
                    for slot, proposal in self.proposals.enumerate():
                        cmdr = Commander(self, slot)
                        proposal['commander'] = cmdr
                        messages.extend(cmdr.start(proposal['command']))

                    return self.tick_timebomb(messages)

            except Preempted as pre:
                if self.read_ballots:
                    self.read_ballots = False
                    while self.ballot < pre.ballot:
                        self.ballot = next(self.ballots)
                    self.scout = Scout(self)
                    return self.scout.start()
                else:
                    self.active = False
                    self.scout = None
                    raise pre

    def _accepted(self, message):
        if self.active:
            slot = message['slot']
            try:
                cmdr = self.proposals[slot]['commander']
            except ValueError:
                return []
            try:
                chosen = cmdr.accepted(message)
                if chosen:
                    command = self.proposals[slot]['command']
                    self.log.info("Proposal for slot %s chosen by %s (%s)",
                                  slot, cmdr, command)
                    del self.proposals[slot]
                    return [{
                        'action': msg.decision,
                        'slot': slot,
                        'command': command,
                        'to': p,
                        }
                        for p in it.chain(self.replicas, self.leaders)]

            except Preempted as pre:
                self.active = False
                raise pre

    def _decision(self, message):
        slot = message['slot']
        self.log.debug("Received decision for %s -> %s from %s",
                       message['slot'], message['command'], message['from'])
        del self.proposals[slot]
        self.decisions[slot] = message['command']

    def _recovered(self, message):
        if message['from'] not in self.acceptors:
            return []
        elif self.active:
            return it.chain(*[p['commander'].recovered(message)
                              for p in self.proposals
                              if p and p['commander']])
        else:
            return self.scout.recovered(message) if self.scout else []

    def deliver(self, *args, **kwargs):
        if self.timebomb and self.timebomb == self.count:
            raise StopIteration
        return super(Leader, self).deliver(*args, **kwargs)
