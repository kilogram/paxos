
import logging

from . import common
from .common import msg
import utils


class Acceptor(common.PaxosActor):
    def __init__(self, hndlr=None):
        self.log = logging.getLogger(__name__)
        self.log.addHandler(hndlr or logging.NullHandler())

        self.ballot = -1
        self.accepted = utils.SparseList()

        self._routes = {
            msg.prepare: self._prepare,
            msg.accept: self._accept,
            # Ignore messages about recovering processes
            msg.recovered: lambda x: [],
        }

    def recover(self, states):
        # No recovery needed
        return []

    def _prepare(self, message):
        if int(message['ballot']) > self.ballot:
            self.log.info("Prepared to receive from %s with ballot %s" %
                          (message['from'], message['ballot']))
            self.ballot = message['ballot']
        return [{
            'action': msg.prepare,
            'to': message['from'],
            'ballot': self.ballot,
            'accepted': self.accepted,
            }]

    def _accept(self, message):
        if message['ballot'] >= self.ballot:
            self.log.info("Accepting ballot %s from %s: %s" %
                          (message['ballot'],
                           message['from'],
                           message['command']))

            self.ballot = message['ballot']
            self.accepted[message['slot']] = common.Accepted(
                message['slot'], message['ballot'], message['command'])

        return [{
            'action': msg.accept,
            'to': message['from'],
            'slot': message['slot'],
            'ballot': self.ballot,
            }]
