
import abc
import collections
import enum


@enum.unique
class msg(enum.IntEnum):
    propose = 1
    prepare = 2
    accept = 3
    decision = 4
    request = 5
    response = 6
    recovered = 7


Accepted = collections.namedtuple('_accepted',
                                  ["slot", "ballot", "command"])


class InvalidAction(Exception):
    pass


class PaxosActor(object, metaclass=abc.ABCMeta):
    def deliver(self, message):
        self.log.debug("Delivering message %s", message)
        try:
            route = self._routes[message['action']]
        except KeyError:
            self.log.error("No route for message action %s: %s" %
                           (message['action'], message))
            raise InvalidAction(message)

        return route(message) or []
