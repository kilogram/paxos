
import itertools as it
import logging

from . import common
from .common import msg
import utils


class Replica(common.PaxosActor):
    def __init__(self, leaders, hndlr=None):
        self.log = logging.getLogger(__name__)
        self.log.addHandler(hndlr or logging.NullHandler())

        self.leaders = leaders

        self.slot_num = 0
        self.proposals = utils.SparseList()
        self.decisions = utils.SparseList()

        self._routes = {
            msg.request: self._request,
            msg.decision: self._decision,
        }

    def recover(self, states):
        def _join(args):
            assert(x == args[0] for x in args)
            return args[0]

        dcns = utils.SparseList.merged(*[utils.SparseList.from_serializable(st)
                                         for st, _ in states])
        self.proposals = utils.SparseList()
        try:
            slot, *args = next(dcns)
            self.decisions = utils.SparseList(
                [_join(args)] + [_join(args) for _, *args in dncs], start=slot)
            self.slot_num = max(slot for _, slot in states)
        except:
            pass

    def get_state(self):
        return (utils.SparseList.serializable(self.decisions), self.slot_num)

    def all_clear(self):
        return (not self.proposals or
                all(p is None for p in self.proposals))

    def _request(self, message):
        self.log.info("Received request: %s", message)
        return self.propose(message['command'])

    def propose(self, command):
        if command in self.decisions:
            return []

        slot = min({
            s for s, prop, decn in utils.SparseList.merged(
                self.proposals, self.decisions)
            if prop is None and decn is None
        } or {max(len(self.decisions), len(self.proposals), self.slot_num)})

        self.log.debug("Proposing %s -> %s", slot, command)

        self.proposals[slot] = command
        return [{
            'action': msg.propose,
            'slot': slot,
            'command': command,
            'to': leader,
            } for leader in self.leaders]

    def _decision(self, message):
        slot = message['slot']
        self.log.info("Decision received for slot %s: %s",
                      slot, message['command'])
        if slot < self.slot_num:
            return []
        assert(self.decisions[slot] in {None, message['command']})
        self.decisions[slot] = message['command']
        responses = []

        while self.decisions[self.slot_num]:
            op = self.decisions[self.slot_num]

            prop = self.proposals.pop(self.slot_num)
            if prop:
                responses.extend(self.propose(prop))

            responses.append({
                'action': msg.response,
                'command': op,
                'slot': self.slot_num,
                })
            self.slot_num += 1

        del self.decisions[:self.slot_num]
        del self.proposals[:self.slot_num]

        return responses
