
import os
import shutil
try:
    import cPickle as pickle
except ImportError:
    import pickle as pickle
import tempfile


class SparseList(list):
    def __init__(self, *args, start=0, **kwargs):
        super(SparseList, self).__init__(*args, **kwargs)
        self._offset = start

    def __setitem__(self, index, value):
        if isinstance(index, slice):
            raise ValueError("Cannot set slice of SparseList")
        index -= self._offset
        missing = index - super(SparseList, self).__len__() + 1
        if missing > 0:
            self.extend([None] * missing)
        list.__setitem__(self, index, value)

    def __getitem__(self, index):
        if isinstance(index, slice):
            if ((index.start is not None and index.start < self._offset)
                    or (index.stop is not None and index.stop < self._offset)):
                raise ValueError("Cannot access deleted element")

            index = slice(index.start - self._offset
                          if index.start is not None else 0,
                          index.stop - self._offset if index.stop else None,
                          None)
        else:
            if (index < self._offset):
                raise ValueError("Cannot access deleted element")
            index -= self._offset

        try:
            return list.__getitem__(self, index)
        except IndexError:
            return None

    def __len__(self):
        return super(SparseList, self).__len__() + self._offset

    def enumerate(self):
        for i, val in enumerate(self):
            yield i + self._offset, val

    def pop(self, index):
        ret = self[index]
        del self[index]
        return ret

    def serializable(self):
        return {'_offset': self._offset,
                '_list': list(self)}

    @classmethod
    def from_serializable(cls, serializable):
        self = cls(serializable['_list'])
        self._offset = serializable['_offset']
        return self

    @classmethod
    def merged(cls, *splists):
        if not all(isinstance(l, cls) for l in splists):
            raise ValueError("Can only merge instances of SparseList")
        if not splists:
            raise StopIteration

        start = max(l._offset for l in splists)

        for i in range(min(l._offset for l in splists), start):
            yield (i, *[None if i < l._offset else l[i] for l in splists])

        for i in range(start, max(len(l) for l in (splists))):
            yield (i, *[l[i] for l in splists])

    def __delitem__(self, index):
        if isinstance(index, slice):
            assert(index.step is None)
            index = slice(max(index.start - self._offset, 0)
                          if index.start is not None else 0,
                          index.stop - self._offset
                          if index.stop is not None else None,
                          None)
        else:
            index = slice(index - self._offset, index - self._offset + 1, None)

        if index.stop <= index.start:
            return

        if index.start > 0:
            super(SparseList, self).__setitem__(
                index, [None] * (index.stop - index.start))
        else:
            super(SparseList, self).__delitem__(index)
            self._offset = (index.stop or index.start + 1) + self._offset


class StableStorage(object):
    def __init__(self, filename):
        self.file = open(filename, 'a+b')

    def recover(self):
        transactions = []

        self.file.seek(0, 0)
        cur_trans = []
        for line in self.file:
            cur_trans.append(line)

            if line == b'\n':
                trans = pickle.loads(b''.join(cur_trans))
                missing = trans['id'] - len(transactions) + 1
                if missing > 0:
                    transactions.extend([None] * missing)
                transactions[trans['id']] = trans
                cur_trans = []

        self.gc(transactions)
        return transactions

    def gc(self, transactions):
        tfile = tempfile.NamedTemporaryFile(mode='w+b',
                                            dir=os.getcwd(),
                                            delete=False)
        for transaction in transactions:
            self.log(transaction, tfile)
        filename = self.file.name
        os.replace(tfile.name, filename)
        self.file = open(filename, 'a+b')
        self.file.seek(0, 2)

    def log(self, transaction, logfile=None):
        logfile = logfile or self.file
        logfile.write(pickle.dumps(transaction) + b"\n\n")
        # Paranoid syncing
        logfile.flush()
        os.fsync(logfile.fileno())
