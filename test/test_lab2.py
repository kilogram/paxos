
import contextlib
import multiprocessing
import queue
import time
import traceback as tb
import unittest

from . import shell
from lib.tblib import pickling_support
pickling_support.install()


def flush_queue(q):
    ret = []
    while True:
        try:
            ret.append(q.get(block=False))
        except queue.Empty:
            break
    return ret


class MultiException(Exception):
    def __init__(self, exceptions):
        _msg = '\n'.join(''.join(tb.format_exception(*exc))
                         for exc in exceptions)
        super(MultiException, self).__init__(_msg)
        self.exceptions = exceptions


class Lab2Test(unittest.TestCase):
    @contextlib.contextmanager
    def _stubs(self):
        exc_q = multiprocessing.Queue()
        sh = shell.Shell(timeout=1, exception_queue=exc_q)
        yield sh
        excs = flush_queue(exc_q)
        sh.shutdown()
        if excs:
            raise MultiException(excs)

    def test_init(self):
        with self._stubs() as shell:
            shell.start(2, 2)

    def test_single_command(self):
        with self._stubs() as shell:
            shell.start(3, 2)
            shell.send_message(0, *['hey', 'there'])
            time.sleep(2)

    def test_multiple_command(self):
        with self._stubs() as shell:
            shell.start(3, 2)
            shell.send_message(0, *['string_one'])
            shell.send_message(0, *['string_two'])
            shell.all_clear()
            chatlog = shell.print(0)
            self.assertCountEqual(
                [
                    (0, 'string_one'),
                    (0, 'string_two'),
                ],
                chatlog)

    def test_kill_servers(self):
        with self._stubs() as shell:
            shell.start(3, 2)
            shell.kill(0)
            shell.send_message(0, *['string_one'])
            shell.all_clear()
            shell.restart(0)
            shell.all_clear()
            shell.send_message(0, *['string_two'])
            shell.all_clear()
            self.assertCountEqual(shell.print(0), shell.print(1))
            chatlog = shell.print(1)
            self.assertCountEqual(
                [
                    (0, 'string_one'),
                    (0, 'string_two'),
                ],
                chatlog)
