
import itertools as it
import logging
import logging.handlers
import multiprocessing
import queue
import sys

import chat


_FMT = ('%(relativeCreated)s - %(name)s(%(process)s:%(threadName)s) '
        '- %(levelname)s - %(message)s')


class Shell(object):
    args = lambda count: (  # noqa
        lambda func: (
            lambda *args, **kwargs: func(*args[:count+1], **kwargs)))

    handlers = {
        'server':       False,
        'client':       False,
        'leader':       False,
        'acceptor':     False,
        'replica':      False,
        'transport':    False,
    }

    def __init__(self, ports=None, timeout=5, exception_queue=None):
        self.ports = iter(ports or it.count(10000))
        self.timeout = timeout
        self.exc_q = exception_queue
        log_q = multiprocessing.Queue()

        nolog = logging.NullHandler()
        qlog = logging.handlers.QueueHandler(log_q)
        self.handlers = {key: qlog if value else nolog
                         for key, value in self.handlers.items()}
        self.log = logging.StreamHandler()
        self.log.setFormatter(logging.Formatter(_FMT))
        self.log_listener = logging.handlers.QueueListener(log_q, self.log)
        self.log_listener.start()

        self.servers, self.clients = None, None

    def shutdown(self):
        for proc in it.chain(self.servers or [], self.clients or []):
            self._kill(proc)
        self.log_listener.stop()

    def _start(self, cls, id, queues):
        try:
            proc = cls(id, self.server_confs, self.client_confs,
                       handlers=self.handlers, queues=queues,
                       timeout=self.timeout)
            proc.run()
        except:
            self.exc_q.put(sys.exc_info())

    def _start_proc(self, id, target):
        queues = multiprocessing.Queue(), multiprocessing.Queue(), self.exc_q
        s = multiprocessing.Process(target=self._start,
                                    args=(target, id, queues))
        s.start()
        queues[1].get(timeout=5)
        return (s, *queues)

    def _cmd(self, p, control, **kwargs):
        if not p:
            return {'response': True}

        p[1].put(dict({
            'type': chat.msg.control,
            'control': control,
        }, **kwargs))
        try:
            resp = p[2].get(timeout=5)
        except queue.Empty:
            resp = ValueError("%s not responding to command" % p[0])
            p[0].terminate()

        if isinstance(resp, Exception):
            raise resp
        elif not resp:
            err = ("Error occured while processing control %s: %s" %
                   control, resp)
            self.log.error(err)
            raise ValueError(err)

        return resp

    def _kill(self, server):
        if not server:
            return None
        try:
            self._cmd(server, 'die')
        except:
            server[0].terminate()
        finally:
            server[0].join(1)
            if server[0].is_alive():
                server[0].terminate()
        return None

    @args(2)
    def start(self, servers, clients):
        all_roles = {chat.role.replica, chat.role.leader, chat.role.acceptor}
        self.server_confs = [
            chat.ServerConfig(i, all_roles, ('127.0.0.1', next(self.ports)))
            for i in range(servers)
        ]
        self.client_confs = [
            chat.ClientConfig(i, ('127.0.0.1', next(self.ports)))
            for i in range(servers, servers + clients)
        ]

        self.servers = [self._start_proc(s.id, chat.Server)
                        for s in self.server_confs]
        self.clients = [self._start_proc(c.id, chat.Client)
                        for c in self.client_confs]

    def send_message(self, from_client, *words):
        assert(0 <= from_client < len(self.clients))
        clt = self.clients[from_client]
        self._cmd(clt, 'send_message', command=' '.join(words))

    @args(1)
    def kill(self, server):
        assert(0 <= server < len(self.servers))
        self.servers[server] = self._kill(self.servers[server])

    @args(1)
    def restart(self, server):
        assert(0 <= server < len(self.servers))
        self._kill(self.servers[server])
        self.servers[server] = self._start_proc(server, chat.Server)

    @args(0)
    def all_clear(self):
        while not all(self._cmd(i, 'all_clear')['response']
                      for i in self.servers):
            pass

    @args(1)
    def time_bomb_leader(self, count):
        return all(self._cmd(i, 'time_bomb_leader', ctrl_args=count)
                   for i in self.servers)

    @args(1)
    def print(self, client):
        assert(0 <= client < len(self.clients))
        cmds = self._cmd(self.clients[client], 'print')['response']
        return [(c - len(self.servers), op) for (c, _, op) in cmds]
