
import os
import tempfile
import unittest
import utils


class TestStableStorage(unittest.TestCase):
    def setUp(self):
        self.test_file = tempfile.NamedTemporaryFile(dir=os.getcwd()).name

    def tearDown(self):
        os.remove(self.test_file)

    def test_store_single(self):
        store = utils.StableStorage(self.test_file)
        store.log({'id': 0, 'dat': "Test entry 0"})
        self.assertEqual(
            [
                {'id': 0, 'dat': "Test entry 0"},
            ],
            store.recover())

    def test_store_multiple(self):
        store = utils.StableStorage(self.test_file)
        store.log({'id': 0, 'dat': "Test entry 0"})
        store.log({'id': 1, 'dat': "Test entry 1"})
        self.assertEqual(
            [
                {'id': 0, 'dat': "Test entry 0"},
                {'id': 1, 'dat': "Test entry 1"},
            ],
            store.recover())

    def test_store_replace(self):
        store = utils.StableStorage(self.test_file)
        store.log({'id': 0, 'dat': "Test entry 0"})
        store.log({'id': 1, 'dat': "Test entry 1"})
        store.log({'id': 0, 'dat': "Test entry 2"})
        self.assertEqual(
            [
                {'id': 0, 'dat': "Test entry 2"},
                {'id': 1, 'dat': "Test entry 1"},
            ],
            store.recover())

    def test_store_restore_twice(self):
        store = utils.StableStorage(self.test_file)
        store.log({'id': 0, 'dat': "Test entry 0"})
        store.log({'id': 1, 'dat': "Test entry 1"})
        store.log({'id': 0, 'dat': "Test entry 2"})
        self.assertEqual(
            [
                {'id': 0, 'dat': "Test entry 2"},
                {'id': 1, 'dat': "Test entry 1"},
            ],
            store.recover())
        self.assertEqual(
            [
                {'id': 0, 'dat': "Test entry 2"},
                {'id': 1, 'dat': "Test entry 1"},
            ],
            store.recover())


class TestSparseList(unittest.TestCase):
    def test_init(self):
        a = utils.SparseList([1, 2, 3])

    def test_access(self):
        a = utils.SparseList([1, 2, 3])
        self.assertEqual(1, a[0])
        self.assertEqual(2, a[1])
        self.assertEqual(3, a[2])

    def test_deletion(self):
        a = utils.SparseList([1, 2, 3])
        del a[0]
        self.assertEqual(2, a[1])
        self.assertEqual(3, a[2])
        with self.assertRaises(ValueError):
            a[0]

        del a[:2]
        self.assertEqual(3, a[2])

        with self.assertRaises(ValueError):
            a[0:3]

        self.assertEqual([3], a[:3])

    def test_slices(self):
        a = utils.SparseList([1, 2, 3, 4, 5, 6])
        del a[:2]
        self.assertEqual(3, a[2])

        with self.assertRaises(ValueError):
            a[0:3]

        self.assertEqual([3], a[:3])

        del a[2:4]
        with self.assertRaises(ValueError):
            a[0:3]

        a[:4]
        self.assertEqual(a[:6], [5, 6])

    def test_set_middle(self):
        a = utils.SparseList([1, 2, 3])
        del a[:2]
        a[5] = 'no'

        self.assertEqual('no', a[5])
        self.assertEqual([3, None, None, 'no'], a)

    def test_pop(self):
        a = utils.SparseList(list(range(10)))
        del a[2:5]
        ret = a.pop(7)

        self.assertEqual([None] * 3, a[2:5])
        self.assertEqual(7, ret)

    def test_delete_middle(self):
        a = utils.SparseList(list(range(100)))
        del a[10:11]
        self.assertEqual(list(range(10)), a[:10])
        self.assertEqual(list(range(11, 100)), a[11:])
        self.assertEqual(None, a[10])

    def test_delete_beginning_middle(self):
        a = utils.SparseList(list(range(10)))
        del a[:4]
        del a[6:8]
        self.assertEqual(list(range(4, 6)), a[4:6])
        self.assertEqual(list(range(8, 10)), a[8:10])

    def test_merged(self):
        a = utils.SparseList(list(range(8)))
        b = utils.SparseList(list(range(10)))
        c = utils.SparseList(list(range(7)))

        del a[:3]
        del a[7]
        del b[:4]
        del b[6:8]
        del c[:2]

        self.assertCountEqual(
            [(2, None, None, 2),
             (3, 3, None, 3),
             (4, 4, 4, 4),
             (5, 5, 5, 5),
             (6, 6, None, 6),
             (7, None, None, None),
             (8, None, 8, None),
             (9, None, 9, None),
             ],
            list(utils.SparseList.merged(a, b, c)))

    def test_serialization(self):
        a = utils.SparseList(list(range(100)))
        del a[10]
        del a[20:30]
        serializable = a.serializable()
        b = utils.SparseList.from_serializable(serializable)
        for i, _a, _b in utils.SparseList.merged(a, b):
            with self.subTest(i=i):
                self.assertEqual(_a, _b)
