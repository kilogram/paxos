
import collections
import itertools as it
import logging
import sys

from paxos import Acceptor, Leader, Replica
from paxos.common import msg as paxos_msg, Accepted

from .transport import Transport
from .common import msg, role, Command, Process, default_handlers


class Server(object):
    def __init__(self, pid, servers, clients, queues=(None, None, None),
                 handlers=None, **kwargs):
        handlers = handlers or default_handlers
        self.log = logging.getLogger(__name__)
        self.log.addHandler(handlers['server'])

        def get_by_role(processes, role):
            return {Process(p.id, role)
                    for p in processes if role in p.roles}
        leaders = get_by_role(servers, role.leader)
        acceptors = get_by_role(servers, role.acceptor)
        replicas = get_by_role(servers, role.replica)

        if not (leaders and acceptors and replicas):
            raise ValueError("Incomplete server configuration")

        self.servers = servers
        self.clients = {Process(p.id, role._client) for p in clients}
        self.replica = (Replica(leaders, hndlr=handlers['replica'])
                        if pid in replicas else None)
        self.acceptor = (Acceptor(hndlr=handlers['acceptor'])
                         if pid in acceptors else None)
        self.leader = (Leader(leaders, acceptors, replicas,
                              it.count(start=pid, step=len(servers)),
                              hndlr=handlers['leader'])
                       if pid in leaders else None)

        self.alive = {*leaders, *acceptors, *replicas}

        if not (self.replica or self.leader or self.acceptor):
            raise ValueError("Useless server")

        cmd_q, self.resp_q, self.exc_q = queues
        if bool(cmd_q) != bool(self.resp_q):
            raise ValueError("Must have valid queues")

        self.pid = pid
        self.transport = Transport(
            self.pid,
            {p.id: p.address for p in it.chain(servers, clients)},
            (cmd_q, self.exc_q),
            hndlr=handlers['transport'],
            **kwargs)
        self.resp_q.put({"response": True})
        self.transport.start()
        self.log.info("Started server %s", self.pid)

    def handle_control(self, message):
        assert(self.resp_q)
        if message['control'] == 'die':
            self.resp_q.put({'response': True})
            raise StopIteration
        elif message['control'] == 'all_clear':
            quorum = (len(set(p.id for p in self.alive)) >
                      len(self.servers) / 2)
            leader_clear = self.leader.all_clear() if self.leader else True
            replica_clear = self.replica.all_clear() if self.replica else True
            self.resp_q.put({'response': not quorum or
                             (leader_clear and replica_clear)})
            return []
        elif message['control'] == 'time_bomb_leader':
            self.resp_q.put({'response': True})
            if self.leader and self.leader.active:
                self.leader.set_timebomb(message['ctrl_args'])
            return []
        elif message['control'] == 'kickoff':
            primary = min(p for p in self.alive if p.role == role.leader)
            if primary == self.pid:
                for out in self.leader.reactivate():
                    out['from'] = Process(self.pid, role.leader)
                    out['type'] = msg.protocol
                    self.send(out)

        else:
            err = ValueError("Unknown control signal")
            self.resp_q.put(err)
            raise err

    def handle_protocol(self, message):
        recv_role = {
            role.acceptor: self.acceptor,
            role.leader: self.leader,
            role.replica: self.replica,
            }[message['to'].role]

        if not recv_role:
            error = "Server is not %s" % (message['to'],)
            self.log.error(error)
            raise ValueError(error)
        outgoing = recv_role.deliver(message)
        for msg in outgoing:
            msg['from'], msg['fromrole'] = self.pid, message['to'].role
        return outgoing

    def handle_timeout(self, message):
        self.log.info("Process timed out! %s", message)
        self.alive.remove(message['from'])
        primary = min(proc for proc in self.alive if proc.role == role.leader)
        if primary == self.pid:
            msgs = self.leader.reactivate()
            for msg in msgs:
                msg['from'] = Process(self.pid, role.leader)
            return msgs
        else:
            return []

    def handle_application(self, message):
        return [{
            'to': client,
            'from': Process(self.pid, role.replica),
            'type': msg.application,
            'slot': message['slot'],
            'command': message['command'],
        } for client in self.clients]

    def handle_recover(self, message):
        p = it.filter(self.servers, lambda x: x.id == message['from'])[0]

        if role.leader in p.roles:
            self.alive |= {Process(p.id, role.leader)}

        # Notify acceptor/replica/leader about new recovery
        return it.chain(*[c.deliver({
            'action': paxos_msg.recovered,
            'from': Process(message['from'], role),
        }) for c in {self.leader, self.acceptor, self.replica}
            for role in p.roles])

    def handle_response(self, message):
        return []

    def recv(self):
        recvd = self.transport.recv()
        if recvd['type'] == msg.control:
            return recvd

        def noop(x):
            return x
        keys = {
            'type': msg,
            'action': paxos_msg,
            'slot': int,
            'ballot': int,
            'accepted': lambda x: [Accepted(s, b, Command(*c))
                                   for s, b, c in x],
            'command': lambda x: Command(*x) if x else x,
            'control': noop,
            'ctrl_args': noop,
        }
        message = {key: keys[key](value)
                   for key, value in recvd.items()
                   if key in keys}
        message['from'] = (Process(recvd['from'], recvd['fromrole'])
                           if 'fromrole' in recvd else recvd['from'])
        message['to'] = (Process(recvd['to'], recvd['torole'])
                         if 'torole' in recvd else recvd['to'])

        if message['type'] == msg.protocol:
            assert(isinstance(message['to'], Process))
            assert(message['to'] == self.pid)
            assert(isinstance(message['from'], Process))
        self.log.debug("Received message %s", message)
        return message

    def send(self, outgoing):
        from_ = outgoing.get('from', self.pid)
        asrole = from_.role if isinstance(from_, Process) else (
            outgoing.get('fromrole', None))
        if (asrole == role.replica
                and outgoing.get('action', None) == paxos_msg.response):
            self.transport.send({
                'to': self.pid,
                'from': self.pid,
                'type': msg.application,
                'slot': outgoing['slot'],
                'command': outgoing['command'],
            })
            return

        def noop(x):
            return x
        keys = {
            'type': int,
            'action': int,
            'slot': int,
            'ballot': int,
            'accepted': list,
            'command': noop,
            'control': noop,
        }
        message = {key: keys[key](value)
                   for key, value in outgoing.items()
                   if key in keys}
        message['from'] = self.pid
        if asrole is not None:
            message['fromrole'] = asrole
        to = outgoing['to']
        if isinstance(to, Process):
            message['to'], message['torole'] = to.id, to.role
        else:
            message['to'] = to

        self.log.debug("Sending message %s", message)
        self.transport.send(message)

    def recover(self):
        # Attempt recovery
        for p in self.servers:
            self.send({
                'from': self.pid,
                'to': p.id,
                'type': msg.recover,
            })

        queued = []
        waitfor = set(s.id for s in self.servers)
        while waitfor:
            message = self.recv()
            if message['type'] not in {msg.timeout, msg.response}:
                queued.append(message)
                continue
            # TODO: Recover state
            waitfor -= {message['from']}

    def run(self, *args, **kwargs):
        try:
            self._run(*args, **kwargs)
        except:
            self.log.exception("Error in server")
            if self.exc_q:
                self.exc_q.put(sys.exc_info())
        finally:
            self.transport.shutdown()

    def _run(self):
        while True:
            message = self.recv()
            if 'from' in message:
                self.alive.add(message['from'])

            try:
                outgoing = {
                    msg.control: self.handle_control,
                    msg.protocol: self.handle_protocol,
                    msg.application: self.handle_application,
                    msg.recover: self.handle_recover,
                    msg.response: self.handle_response,
                    msg.timeout: self.handle_timeout,
                }[message['type']](message)

            except KeyError as kerr:
                raise ValueError("Invalid message type for server: %s"
                                 % message) from kerr

            except StopIteration:
                break

            for out in outgoing:
                out['type'] = out.get('type', msg.protocol)
                self.send(out)
