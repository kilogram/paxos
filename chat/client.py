
import logging
import sys

from paxos.common import msg as paxos_msg
from .common import msg, role, Command, Process, default_handlers
from .transport import Transport
import utils


class Client(object):
    def __init__(self, pid, servers, clients, queues=(None, None),
                 handlers=None, **kwargs):
        handlers = handlers or default_handlers
        self.log = logging.getLogger(__name__)
        self.log.addHandler(handlers['client'])
        self.replicas = {Process(s.id, role.replica)
                         for s in servers if role.replica in s.roles}
        self.pid = pid
        self.chatlog = utils.SparseList()

        cmd_q, self.resp_q, self.exc_q = queues
        if bool(cmd_q) != bool(self.resp_q):
            raise ValueError("Must have valid queues")

        self.rsn = 0

        addresses = {p.id: p.address for p in servers
                     if role.replica in p.roles}
        addresses[self.pid] = [c for c in clients
                               if c.id == self.pid][0].address
        self.transport = Transport(pid, addresses,
                                   (cmd_q, self.exc_q),
                                   hndlr=handlers['transport'],
                                   **kwargs)
        self.resp_q.put({"response": True})
        self.transport.start()

    def handle_control(self, message):
        assert(self.resp_q)
        self.log.info("Received control signal %s", message['control'])
        if message['control'] == 'die':
            self.resp_q.put({'response': True})
            raise StopIteration
        elif message['control'] == 'send_message':
            self.resp_q.put({'response': True})
            self.rsn += 1
            return [{
                'to': i,
                'type': msg.protocol,
                'action': paxos_msg.request,
                'command': (self.pid, self.rsn, message['command']),
            } for i in self.replicas
            ]
        elif message['control'] == 'print':
            self.resp_q.put({'response': [msg for msg in self.chatlog if msg]})
            return []
        else:
            err = ValueError("Unknown control signal")
            self.resp_q.put(err)
            raise err

    def recv(self):
        recvd = self.transport.recv()

        if recvd['type'] == msg.control:
            return recvd

        def noop(x):
            return x
        keys = {
            'type': msg,
            'action': paxos_msg,
            'command': lambda x: Command(*x) if x else x,
            'slot': int,
            'control': noop,
        }
        message = {key: keys[key](value)
                   for key, value in recvd.items()
                   if key in keys}
        message['from'] = (Process(recvd['from'], recvd['fromrole'])
                           if 'fromrole' in recvd else recvd['from'])
        message['to'] = (Process(recvd['to'], recvd['torole'])
                         if 'torole' in recvd else recvd['to'])
        self.log.debug("Received message %s" % message)
        return message

    def send(self, outgoing):
        def noop(x):
            return x
        keys = {
            'type': int,
            'action': int,
            'command': noop,
            'control': noop,
        }
        message = {key: keys[key](value)
                   for key, value in outgoing.items()
                   if key in keys}
        message['from'], message['fromrole'] = self.pid, role._client
        to = outgoing['to']
        message['to'], message['torole'] = to.id, to.role

        self.log.debug("Sending message %s", message)
        self.transport.send(message)

    def run(self, *args, **kwargs):
        try:
            self._run(*args, **kwargs)
        except:
            self.log.exception("Error in client")
            if self.exc_q:
                self.exc_q.put(sys.exc_info())
        finally:
            self.transport.shutdown()

    def _run(self):
        while True:
            self.log.info("Hey")
            message = self.recv()
            if message['type'] == msg.timeout:
                outgoing = []

            elif message['type'] == msg.control:
                try:
                    outgoing = self.handle_control(message)
                except StopIteration:
                    break

            elif (message['type'] == msg.application
                  and message['to'].role == role._client):
                # TODO: Implement chat log
                self.chatlog[message['slot']] = message['command']
                outgoing = []

            else:
                raise ValueError("Invalid message type for client: %s"
                                 % message)

            for out in outgoing:
                self.send(out)
