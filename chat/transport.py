

import json
import logging
import os
import queue
import socket
import sys
import time
import threading

from .common import msg


class SafeThread(threading.Thread):
    def __init__(self, exception_queue, *args, **kwargs):
        super(SafeThread, self).__init__(*args, **kwargs)
        self.exc_q = exception_queue

    def run(self, *args, **kwargs):
        try:
            super(SafeThread, self).run(*args, **kwargs)
        except:
            self.exc_q.put(sys.exc_info())
            return


class Transport(object):
    def __init__(self, pid, addresses,
                 queues=(None, None),
                 timeout=5, hndlr=None):

        self.pid = pid
        self.addresses = addresses
        self.timeout = timeout

        self.log = logging.getLogger(__name__)
        self.log.addHandler(hndlr or logging.NullHandler())

        self.send_queue, self.recv_queue = queue.Queue(), queue.Queue()
        self.delayed_send_queue = queue.Queue()
        self.cmd_queue, self.exc_q = queues
        self.stop = threading.Event()

        self.send_thread = SafeThread(
            self.exc_q,
            target=self.send_thread, name='send_thread',
            args=(self.send_queue, addresses))
        self.recv_thread = SafeThread(
            self.exc_q,
            target=self.recv_thread, name='recv_thread',
            args=(self.recv_queue, addresses))
        self.cmd_thread = SafeThread(
            self.exc_q,
            target=self.cmd_thread, name='cmd_thread',
            args=(self.cmd_queue, self.recv_queue))
        self._heartbeat = threading.Timer(self.timeout / 2, self.heartbeat,
                                          args=(self.send_queue, addresses))

    def start(self):
        self._heartbeat.start()
        self.send_thread.start()
        self.recv_thread.start()
        self.cmd_thread.start()

        cur_time = time.time()
        unstarted_procs = set(self.addresses) - {self.pid}
        to_deliver = []
        while (unstarted_procs and
               (time.time() - cur_time) < (self.timeout * 5)):
            for proc in unstarted_procs:
                self.send({'from': self.pid, 'to': proc,
                           'type': msg.init})
            try:
                pkt = self.recv_queue.get(timeout=1.0)
            except queue.Empty:
                pkt = None
            if pkt and 'from' in pkt and pkt['from'] in unstarted_procs:
                self.send({'from': self.pid, 'to': pkt['from'],
                           'type': msg.init})
                unstarted_procs -= {pkt['from']}
            if pkt and pkt['type'] not in {msg.init,
                                           msg.init_ack,
                                           msg.timeout}:
                to_deliver.append(pkt)

        # Consider any unstarted processes to be timed out
        for proc in unstarted_procs:
            self.log.warning("Process did not come up: %s at %s:%d" %
                             (proc,
                              self.addresses[proc][0],
                              self.addresses[proc][1]))
            self.recv_queue.put({'from': proc, 'to': self.pid, 'type':
                                 msg.timeout})
        for pkt in to_deliver:
            self.recv_queue.put(pkt)
        self.log.info("Transport started")

    def shutdown(self):
        self._heartbeat.cancel()
        self.stop.set()
        self.send_queue.put(None)
        self.cmd_queue.put(None)
        self.recv_thread.join()
        self.send_thread.join()
        self.cmd_thread.join()

    def heartbeat(self, queue, addresses):
        for proc in addresses:
            if proc == self.pid:
                continue
            queue.put({'from': self.pid, 'to': proc,
                       'type': msg.heartbeat})
        self._heartbeat = threading.Timer(self.timeout / 2, self.heartbeat,
                                          args=(queue, addresses))
        self._heartbeat.start()

    def send_thread(self, queue, addresses):
        send_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        while True:
            pkt = queue.get()
            if self.stop.is_set():
                return
            if 'type' not in pkt:
                self.log.critical("very bad %s", pkt)
            elif pkt['type'] not in {msg.init, msg.init_ack, msg.heartbeat}:
                self.log.debug("Outgoing message %s" % pkt)
            send_sock.sendto(json.dumps(pkt).encode(), addresses[pkt['to']])

    def cmd_thread(self, in_queue, out_queue):
        if not in_queue:
            return

        while True:
            pkt = in_queue.get()
            if self.stop.is_set():
                return
            out_queue.put(pkt)

    def recv_thread(self, queue, addresses):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.settimeout(self.timeout)
        sock.bind(addresses[self.pid])

        cur_time = time.time()
        last_heard = {proc: cur_time for proc in addresses if proc != self.pid}

        while True:
            if self.stop.is_set():
                return

            cur_time = time.time()
            dead_procs = set(p for p, t in last_heard.items()
                             if cur_time - t > self.timeout)
            for dead_proc in dead_procs:
                last_heard.pop(dead_proc)
                queue.put({'from': dead_proc, 'to': self.pid,
                           'type': msg.timeout})
            try:
                data, addr = sock.recvfrom(8192)
            except socket.timeout:
                pass
            else:
                pkt = json.loads(data.decode())
                if pkt['type'] == msg.init:
                    self.send_queue.put({'from': self.pid, 'to': pkt['from'],
                                         'type': msg.init_ack})
                    queue.put(pkt)
                elif pkt['type'] in {msg.heartbeat, msg.init_ack}:
                    pass
                else:
                    self.log.debug("Incoming message %s" % pkt)
                    queue.put(pkt)
                last_heard[pkt['from']] = time.time()
                last_heard.pop(self.pid, None)

    def send(self, message):
        if 'type' not in message:
            self.log.error("Untyped message: %s", message)
            raise ValueError("Untyped message: %s" % message)
        if message['to'] == self.pid:
            self.log.debug("Short-circuting message %s", message)
            self.recv_queue.put(json.loads(json.dumps(message)))
        else:
            self.send_queue.put(message)

    def recv(self, timeout=None):
        while True:
            try:
                pkt = self.recv_queue.get(block=True, timeout=timeout)
                if pkt['type'] not in {msg.heartbeat, msg.init, msg.init_ack}:
                    return pkt
            except queue.Empty:
                return None
