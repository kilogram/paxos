
import contextlib
import unittest
import unittest.mock as mock

from paxos import msg as paxos_msg
from chat import client, role, Process, ServerConfig, ClientConfig
from chat.common import msg


class ClientTest(unittest.TestCase):
    servers = [
        ServerConfig(0, {role.leader, role.replica, role.acceptor}, 0),
        ServerConfig(1, {role.acceptor, role.replica}, 1),
        ServerConfig(2, {role.leader, role.replica}, 2),
        ServerConfig(3, {role.acceptor}, 3),
        ]
    clients = [
        ClientConfig(4, 0),
        ClientConfig(5, 1),
        ClientConfig(6, 2),
    ]

    @contextlib.contextmanager
    def _stubs(self, messages, id=4):
        with mock.patch('chat.client.Transport') as Transport:
            transport = Transport()
            transport.recv.side_effect = messages
            queues = mock.Mock(), mock.Mock(), mock.Mock()
            yield (client.Client(id, self.servers, self.clients,
                                 queues=queues),
                   transport, queues[1])

    def _msg_enc(self, from_, to, type_, **kwargs):
        def noop(x):
            return x

        message = dict({
            'from': Process(*from_),
            'to': Process(*to),
            'type': type_,
            }, **kwargs)

        keys = {
            'type': int,
            'from': noop,
            'to': noop,
            'action': int,
            'command': noop,
            'control': noop,
        }
        message = {key: keys[key](value)
                   for key, value in message.items()
                   if key in keys}
        message['from'], message['fromrole'] = message.pop('from')
        message['to'], message['torole'] = message.pop('to')
        return message

    def _msg(self, from_, to, type_, **kwargs):
        msg = dict({
            'from': Process(*from_),
            'to': Process(*to),
            'type': type_,
            }, **kwargs)
        return msg

    def test_init(self):
        with self._stubs([]) as (clt, transport, q):
            pass

    def test_stop(self):
        messages = [
            self._msg((0, role._client), (0, role._client), msg.control,
                      control='die'),
        ]
        with self._stubs(messages) as (clt, transport, q):
            clt.run()
            q.put.assert_called_once_with({'response': True})

    def test_request(self):
        messages = [
            self._msg((4, role._client), (4, role._client), msg.control,
                      control='send_message', command='hello'),
            self._msg((4, role._client), (4, role._client), msg.control,
                      control='die'),
        ]

        output = [
            self._msg_enc((4, role._client), (i, role.replica), msg.protocol,
                          action=paxos_msg.request,
                          command=(4, 1, 'hello'))
            for i in {0, 1, 2}
        ]

        with self._stubs(messages) as (clt, transport, q):
            clt.run()
            self.assertCountEqual([mock.call(o) for o in output],
                                  transport.send.mock_calls)
