
import contextlib
import unittest
import unittest.mock as mock

from paxos import msg as paxos_msg
from chat import server, role, Process, ServerConfig, ClientConfig
from chat.common import msg, Command


class ServerTest(unittest.TestCase):
    servers = [
        ServerConfig(0, {role.leader, role.replica}, 0),
        ServerConfig(1, {role.acceptor, role.replica}, 1),
        ServerConfig(2, {role.leader, role.replica, role.acceptor}, 2),
        ServerConfig(3, {role.acceptor}, 3),
        ]
    clients = [
        ClientConfig(4, 0),
        ClientConfig(5, 1),
        ClientConfig(6, 2),
    ]

    cmds = [
        Command(2, 0, 'cmd0'),
        Command(1, 0, 'cmd1'),
        Command(1, 1, 'cmda'),
        Command(2, 1, 'cmdb'),
    ]

    @contextlib.contextmanager
    def _stubs(self, messages, id=2):
        with mock.patch('chat.server.Transport') as Transport:
            transport = Transport()
            transport.recv.side_effect = messages
            queues = mock.Mock(), mock.Mock(), mock.Mock()
            yield (server.Server(id, self.servers, self.clients,
                                 queues=queues),
                   transport, queues[1])

    def _msg_enc(self, from_, to, type_, **kwargs):
        def noop(x):
            return x

        message = dict({
            'from': Process(*from_),
            'to': Process(*to),
            'type': type_,
            }, **kwargs)

        keys = {
            'type': int,
            'from': noop,
            'to': noop,
            'action': int,
            'slot': int,
            'ballot': int,
            'accepted': list,
            'command': noop,
            'control': noop,
        }
        message = {key: keys[key](value)
                   for key, value in message.items()
                   if key in keys}
        message['from'], message['fromrole'] = message.pop('from')
        message['to'], message['torole'] = message.pop('to')
        return message

    def _msg(self, from_, to, type_, **kwargs):
        msg = dict({
            'from': Process(*from_),
            'type': type_,
            }, **kwargs)
        msg['to'], msg['torole'] = to
        return msg

    def test_init(self):
        messages = [
            self._msg((4, role._client), (2, role.replica), msg.protocol,
                      action=paxos_msg.request, command=self.cmds[0]),
            self._msg((4, role._client), (2, role.replica), msg.control,
                      control='die'),
        ]
        with self._stubs(messages) as (srv, transport, q):
            pass

    def test_stop(self):
        messages = [
            self._msg((4, role._client), (2, role.replica), msg.control,
                      control='die'),
        ]
        with self._stubs(messages) as (srv, transport, q):
            srv.run()
            q.put.assert_called_once_with(
                {'response': True})

    def test_replica(self):
        messages = [
            self._msg((4, role._client), (2, role.replica), msg.protocol,
                      action=paxos_msg.request, command=self.cmds[0]),
            self._msg((0, role.leader), (2, role.replica), msg.protocol,
                      action=paxos_msg.decision, slot=0, command=self.cmds[0]),
            self._msg((2, role.replica), (2, role.replica), msg.application,
                      command=self.cmds[0], slot=0),
            self._msg((4, role._client), (2, role.replica), msg.control,
                      control='die'),
        ]
        output = [
            self._msg_enc((2, role.replica), (i, role.leader), msg.protocol,
                          action=paxos_msg.propose, slot=0,
                          command=self.cmds[0])
            for i in {0, 2}
        ] + [
            {
                'type': msg.application,
                'to': 2,
                'from': 2,
                'slot': 0,
                'command': self.cmds[0],
            }
        ] + [
            self._msg_enc((2, role.replica), (i, role._client),
                          msg.application, command=self.cmds[0], slot=0)
            for i in {4, 5, 6}
        ]

        with self._stubs(messages) as (srv, transport, q):
            srv.run()
            self.assertCountEqual(
                map(mock.call, output), transport.send.mock_calls)

    def test_leader(self):
        messages = [
            self._msg((0, role.replica), (2, role.leader), msg.protocol,
                      action=paxos_msg.propose, slot=0, command=self.cmds[0]),
            self._msg((2, role.replica), (2, role.leader), msg.protocol,
                      action=paxos_msg.propose, slot=0, command=self.cmds[0]),
        ] + [{
            'from': 0,
            'to': 2,
            'type': msg.timeout,
        }] + [
            self._msg((4, role._client), (2, role.replica), msg.control,
                      control='die'),
        ]

        output = [
            self._msg_enc((2, role.leader), (i, role.acceptor), msg.protocol,
                          action=paxos_msg.prepare, ballot=6)
            for i in {1, 2, 3}
        ]

        with self._stubs(messages) as (srv, transport, q):
            srv.run()
            self.assertCountEqual(
                map(mock.call, output), transport.send.mock_calls)

    def test_acceptor(self):
        messages = [
            self._msg((0, role.leader), (2, role.acceptor), msg.protocol,
                      action=paxos_msg.prepare, ballot=0),
            self._msg((2, role.leader), (2, role.acceptor), msg.protocol,
                      action=paxos_msg.prepare, ballot=2),
            self._msg((0, role.leader), (2, role.acceptor), msg.protocol,
                      action=paxos_msg.accept, ballot=0,
                      slot=0, command=self.cmds[0]),
            self._msg((2, role.leader), (2, role.acceptor), msg.protocol,
                      action=paxos_msg.accept, ballot=2,
                      slot=0, command=self.cmds[1]),
            self._msg((4, role._client), (2, role.replica), msg.control,
                      control='die'),
        ]

        output = [
            self._msg_enc((2, role.acceptor), (0, role.leader), msg.protocol,
                          action=paxos_msg.prepare, ballot=0, accepted=[]),
            self._msg_enc((2, role.acceptor), (2, role.leader), msg.protocol,
                          action=paxos_msg.prepare, ballot=2, accepted=[]),
            self._msg_enc((2, role.acceptor), (0, role.leader), msg.protocol,
                          action=paxos_msg.accept, ballot=2, slot=0),
            self._msg_enc((2, role.acceptor), (2, role.leader), msg.protocol,
                          action=paxos_msg.accept, ballot=2, slot=0),
        ]

        with self._stubs(messages) as (srv, transport, q):
            srv.run()
            self.assertCountEqual(
                map(mock.call, output), transport.send.mock_calls)

    def test_recovery(self):
        messages = [
            {'to': 2, 'from': i, 'type': msg.response}
            for i in range(4)
        ]

        output = [
            {'to': i, 'from': 2, 'type': msg.recover}
            for i in range(4)
        ]

        with self._stubs(messages) as (srv, transport, q):
            srv.recover()
            self.assertCountEqual(
                map(mock.call, output), transport.send.mock_calls)
