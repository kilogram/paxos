
import collections
import contextlib
import unittest

from chat import role, Process, transport


_local = '127.0.0.1'


def get_sock(addr, timeout=5):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.settimeout(timeout)
    sock.bind(addr)
    return sock


class TransportTest(unittest.TestCase):
    others = (
        Process(0, role._client),  (_local, 10000),
        Process(1, role._client),  (_local, 10001),
        Process(2, role._client),  (_local, 10002),
        Process(0, role.acceptor), (_local, 10003),
        Process(0, role.replica),  (_local, 10004),
        Process(0, role.leader),   (_local, 10005),
        Process(1, role.leader),   (_local, 10006),
        Process(1, role.acceptor), (_local, 10007),
        Process(2, role.leader),   (_local, 10008),
        Process(2, role.acceptor), (_local, 10009),
        Process(2, role.replica),  (_local, 10010),
    )

    def setUp(self):
        self.sockets = [get_sock(addr) for _, addr in self.others]

    @contextlib.contextmanager
    def _stubs(self, cmd_q, client=False):
        yield transport.Transport(
            self.others[0] if client else self.others[-1],
            collections.OrderedDict(self.others),
            command_queue=cmd_q, timeout=1)

    # TODO: Write tests for transport
