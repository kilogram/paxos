
import collections
import enum
import logging


@enum.unique
class msg(enum.IntEnum):
    # Network messages
    timeout = 0
    init = 1
    init_ack = 2
    heartbeat = 3
    control = 4
    response = 5
    protocol = 6
    application = 7
    recover = 8


@enum.unique
class role(enum.IntEnum):
    replica = 0
    leader = 1
    acceptor = 2
    _client = 3


class Command(collections.namedtuple('_command',
                                     ["client", "seq", "operation"])):
    def __eq__(a, b):
        return a.client == b.client and a.seq == b.seq

    def __ne__(a, b):
        return not a.__eq__(b)

    def __hash__(self):
        return hash(self.client) * hash(self.seq)


class Process(collections.namedtuple('_client', ['id', 'role'])):
    def __new__(cls, id, role_):
        if not isinstance(id, int):
            raise ValueError("Invalid process id %s" % (id,))
        return super(Process, cls).__new__(cls, id, role(role_))

    def __eq__(proc, other):
        if isinstance(other, Process):
            return proc.id == other.id and proc.role == other.role
        else:
            return proc.id == other

    def __hash__(self):
        return hash(self.id) * (
            hash(self.role) if self.role == role._client else 1)


class ClientConfig(collections.namedtuple('_client', ['id', 'address'])):
    def __eq__(proc, other):
        if isinstance(other, ClientConfig):
            return proc.id == other.id
        elif isinstance(other, ServerConfig):
            return False
        else:
            return proc.id == other

    def __hash__(self):
        return hash(self.id) * hash(self.address)


class ServerConfig(collections.namedtuple('_process',
                                          ['id', 'roles', 'address'])):
    def __new__(cls, id, roles, address):
        roles = frozenset(roles)
        if not all(isinstance(r, role) for r in roles):
            raise ValueError("Invalid roles for process: %s", roles)
        return super(ServerConfig, cls).__new__(cls, id, roles, address)

    def __hash__(self):
        return hash(self.id) * hash(self.address)

    def __eq__(proc, other):
        if isinstance(other, ServerConfig):
            return proc.id == other.id
        elif isinstance(other, ClientConfig):
            return False
        else:
            return proc.id == other


default_handlers = collections.defaultdict(logging.NullHandler)
