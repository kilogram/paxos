#! /usr/bin/env python3

import fileinput


def parselines(lines):
    for line in lines:
        cmd, *args = line.split()
        translation = {
            'start': 'start',
            'sendMessage': 'send_message',
            'crashServer': 'kill',
            'restartServer': 'restart',
            'allClear': 'all_clear',
            'timeBombLeader': 'time_bomb_leader',
            'printChatLog': 'print',
        }[cmd]

        yield translation, args

if __name__ == "__main__":
    commands = parselines(fileinput.input())
    print(list(commands))
